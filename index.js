const electron = require('electron');

const { app, BrowserWindow, Menu, ipcMain } = electron;

let mainWindow;

app.on('ready', () => {
  mainWindow = new BrowserWindow({ webPreferences: {
            nodeIntegration: true
        }}
);
  mainWindow.loadURL(`file://${__dirname}/main.html`);
  mainWindow.on('closed', () => app.quit());
  let mainMenu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(mainMenu);
});

ipcMain.on('todo:submit', (event, todo) => {
  mainWindow.webContents.send('todo:list', todo);
  addWindow.close();
})

const isDarwin = process.platform === 'darwin';

function createAddWindow() {
  addWindow = new BrowserWindow({
    width: 300,
    height: 200,
    title: 'Add New Todo',
    webPreferences: { nodeIntegration: true }
  });
  addWindow.loadURL(`file://${__dirname}/addTodo.html`);
  addWindow.on('closed', () => {
    addWindow = null;
  })
}

const menuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'New Todo',
        accelerator: isDarwin ? 'Command+N' : 'Ctrl+N',
        click() {
          createAddWindow();
        }
      },
      {
        label: 'Clear Todo',
        accelerator: isDarwin ? 'Command+E' : 'Ctrl+E',
        click() {
          // mainWindow.reload(); //one type of solution
          mainWindow.webContents.send('todo:clear'); //another solution
        }
      },
      {
        label: 'Quit',
        accelerator: isDarwin ? 'Command+Q' : 'Ctrl+Q',
        click() {
          app.quit();
        }
      }
    ]
  }
];

if (isDarwin) {
  menuTemplate.unshift({label: ''});
}

if (process.env.NODE_ENV !== 'production') {
  menuTemplate.push({
    label: 'View',
    submenu: [
      { role: 'reload' },
      {
        label: 'Toggle developer tools',
        accelerator: isDarwin ? 'Command+i' : 'Ctrl+i',
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        }
      }
    ]
  })
}
